const env = process.env.NODE_ENV

export default {
  ssr: true,
  target: 'static',
  modern: env !== 'development',
  server: {
    host: '0'
  },
  head: {
    title: 'Andrea Faggin - web developer',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'mobile-web-app-capable',
        content: 'true'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      },
      {
        hid: 'og:title',
        property: 'og:title',
        name: 'og:title',
        content: 'Andrea Faggin - web developer'
      },
      {
        hid: 'og:description',
        property: 'og:description',
        name: 'og:description',
        content: 'Hi! I\'m Andrea, i do web stuff.'
      },
      {
        hid: 'og:host',
        property: 'og:host',
        name: 'og:host',
        content: 'https://andreafagg.in/'
      },
      {
        hid: 'application-name',
        property: 'application-name',
        name: 'application-name',
        content: 'Andrea Faggin - web developer'
      },
      {
        hid: 'theme-color',
        property: 'theme-color',
        name: 'theme-color',
        content: '#F29154'
      },
      {
        hid: 'msapplication-TileColor',
        property: 'msapplication-TileColor',
        name: 'msapplication-TileColor',
        content: '#F29154'
      },
      {
        hid: 'msapplication-TileImage',
        property: 'msapplication-TileImage',
        name: 'msapplication-TileImage',
        content: '/favicon/mstile-144x144.png'
      },
      {
        hid: 'format-detection',
        property: 'format-detection',
        name: 'format-detection',
        content: 'telephone=no'
      }
    ],
    link: [{
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/favicon/apple-touch-icon.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon/favicon-32x32.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon/favicon-16x16.png'
      },
      {
        rel: 'shortcut icon',
        href: '/favicon/favicon.ico'
      }
    ]
  },
  loading: {
    color: '#F29154'
  },
  styleResources: {
    scss: [
      '~node_modules/sass-mq/_mq.scss',
      '~/assets/stylesheets/_variables.scss',
      '~/assets/stylesheets/_mixins.scss'
    ]
  },
  css: [
    '~/assets/stylesheets/main.scss'
  ],
  pwa: {
    icon: {
      sizes: [64, 120, 144, 152, 192, 256]
    },
    workbox: {
      config: {
        debug: env !== 'production'
      },
      runtimeCaching: [{
          urlPattern: 'https://fonts.googleapis.com/.*',
          handler: 'cacheFirst',
          method: 'GET',
          strategyOptions: {
            cacheableResponse: {
              statuses: [0, 200]
            }
          }
        },
        {
          urlPattern: 'https://fonts.gstatic.com/.*',
          handler: 'cacheFirst',
          method: 'GET',
          strategyOptions: {
            cacheableResponse: {
              statuses: [0, 200]
            }
          }
        }
      ]
    },
    meta: false
  },
  purgeCSS: {
    mode: 'postcss',
    whitelist: [
      '__nuxt',
      '__layout',
      '__fill'
    ]
  },
  plugins: [{
    src: '~plugins/jsonld.js'
  }],
  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/sitemap',
    '@nuxtjs/style-resources',
    '@nuxtjs/amp',
    'nuxt-purgecss'
  ],
  sitemap: {
    hostname: 'https://andreafagg.in'
  },
  render: {
    http2: {
      push: true
    }
  },
  build: {
    parallel: true,
    babel: {
      plugins: [
        '@babel/plugin-proposal-optional-chaining'
      ]
    }
  }
}
