const pkg = require('./package')
module.exports = {
  apps: [
    {
      name: pkg.name,
      script: './node_modules/.bin/nuxt',
      args: 'start',
      cwd: `${process.env.PWD}/current`,
      autorestart: true,
      exec_mode: 'cluster',
      instances: 4,
      watch: false,
      max_memory_restart: '1G'
    }
  ]
}
