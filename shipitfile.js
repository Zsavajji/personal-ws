const pkg = require('./package')

module.exports = shipit => {
  require('shipit-deploy')(shipit)

  shipit.initConfig({
    default: {
      repositoryUrl: pkg.repository.url,
      keepReleases: 3,
      ignores: ['.nuxt', '.git', 'node_modules'],
      deleteOnRollback: false,
    },
    production: {
      deployTo: '/var/www/html/site',
      servers: `${pkg.deploy.user}@${pkg.deploy.host}`,
      branch: 'master'
    }
  })

  // TASKS DEFINITION

  shipit.blTask('clean', async () => {
    await shipit.remote(`cd ${shipit.releasePath} && rm -rf ./node_modules ./.nuxt`)
  })

  shipit.blTask('build', async () => {
    await shipit.remote(`cd ${shipit.releasePath} && yarn install && NODE_ENV=production yarn run generate`)
  })

  shipit.task('start', async () => {
    await shipit.remote(`cd ${shipit.config.deployTo} && NODE_ENV=production pm2 startOrReload current/ecosystem.config.js --env production --update-env`)
    shipit.emit('started')
  })

  // EVENTS

  shipit.on('updated', () => {
    shipit.start('clean', 'build')
  })

  shipit.on('deployed', () => {
    shipit.start('start')
  })

  shipit.on('rollbacked', () => {
    shipit.start('start')
  })
}
